var config = {
  entry: './main.js',

  output: {
    path:'./',
    filename: 'index.js',
  },

  resolve: {
    extensions: ['', '.js', '.jsx']
  },

  node: {
    net: "empty",
    tls: "empty",
    fs: "empty",
    dns: "empty"
  },

  devServer: {
    inline: true,
    port: 3000
  },

  module: {
    loaders: [
      {
        test: /\.jsx?$/,
        exclude: /node_modules/,
        loader: 'babel-loader',

        query: {
           presets: ['es2015', 'react']
        }
      },
      // {
      //   test: /\.css$/,
      //   loader: "style-loader!css-loader"
      // },
      {
        test: /\.json$/,
        loader: 'json'
      },
      {
        test:   /\.md/,
        loader: 'markdown-it'
      }
    ]
  }
}

module.exports = config;
