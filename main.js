import React from 'react';
import ReactDOM from 'react-dom';
import App from './views/app.jsx';
import { createStore } from 'redux';
import {TemperatureModel} from './models/temperatureModel';
import reducer from './src/reducer';
import { Provider } from 'react-redux';
import {DataRetriever} from './src/dataRetriever';

const store = createStore(reducer);

var retriever = new DataRetriever();
retriever.obtainInformation(dispatchState);

ReactDOM.render(
                <Provider store={store}>
                  <App />
                </Provider>,
                document.getElementById('app')
              );

function dispatchState(tempMpdel) {
  store.dispatch({type: 'NEW_DATE', model: tempMpdel});
}
