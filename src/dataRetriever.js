import {TemperatureModel} from '../models/temperatureModel'

var request = require('request');
var dateFormat = require('dateformat');

class DataRetriever {

  constructor() {
    this.model = new TemperatureModel();
  }

  // Make a API to obtain the temperature
  // If date is null, get information for today
  obtainInformation(callback, date = null) {
    var obj = this;

    // If no date provided, make call for today
    if(date == null) {
      console.log('here');
      request({
                method: 'GET',
                url: 'http://marsweather.ingenology.com/v1/latest/?format=json',
                headers: [
                  {
                    name: 'Access-Control-Allow-Origin',
                    value: '*'
                  },
                  {
                    name: 'Access-Control-Allow-Methods',
                    value: 'GET, POST, PATCH, PUT, DELETE, OPTIONS'
                  },
                  {
                    name: 'Access-Control-Allow-Headers',
                    value: 'Origin, Content-Type, X-Auth-Token'
                  },
                  {
                    name: 'Access-Control-Allow-Credentials',
                    value: 'true'
                  }
                ]
              },
              function (error, response, body) {
        if (!error && response.statusCode == 200) {
          obj.transformResponseObject(body);
          callback(obj.model);
        }
      });
    }
    // if date provided, make archive call
    else {
      request('http://marsweather.ingenology.com/v1/archive/?terrestrial_date_start=' +
                  dateFormat(date, 'yyyy-mm-dd') + '&terrestrial_date_end=' +
                  dateFormat(date, 'yyyy-mm-dd'), function (error, response, body) {
        if (!error && response.statusCode == 200) {
          obj.transformResultsObject(body);
          callback(obj.model);
        }
      });
    }
  }

  // Transforms the JSON object received for one date into the model
  transformResponseObject(response) {
    var reportBody = JSON.parse(response).report;

    this.parseTemperatureObject(reportBody);
  }

  // Transforms and array of results, containing one or zero objects
  transformResultsObject(response) {
    var results = JSON.parse(response).results;

    // If we have results for the searched day
    if(results.length == 1) {
      var reportBody = results[0];

      this.parseTemperatureObject(reportBody);
    }
    // If no result is returned for the searched day
    else {
      this.model.valid = false;
    }
  }

  parseTemperatureObject(temp) {
    this.model.date = temp.terrestrial_date;
    this.model.temperature = (temp.min_temp + temp.max_temp) / 2;
    this.model.sky = temp.atmo_opacity;
    this.model.valid = true;
  }
}

module.exports.DataRetriever = DataRetriever;
