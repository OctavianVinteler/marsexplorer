//import {DatabaseInterface} from './databaseInterface';
//import {DataRetriever} from './dataRetriever';

//  var database = new DatabaseInterface();
//var retriever = new DataRetriever();

const reducer = (state = {sky: "N/A", temperature: 0, date: "N/A"}, action) => {
  switch (action.type) {
    case 'NEW_DATE':
      return {sky: action.model.sky, temperature: action.model.temperature, date: action.model.date};
    default:
      return state;
  }
};

export default reducer;
