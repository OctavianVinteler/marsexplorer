import { connect } from 'react-redux';
import Content from '../views/content';

const mapStateToProps = (state) => {
  return {
    date: state.date,
    sky: state.sky,
    temperature: state.temperature
  };
}

const mapDispatchToProps = (dispatch) => {
  return {
    // onTodoClick: (id) => {
    //   dispatch(toggleTodo(id))
    // }
  };
}

const ContentContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(Content)

export default ContentContainer
