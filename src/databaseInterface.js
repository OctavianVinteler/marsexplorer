import {TemperatureModel} from '../models/temperatureModel';
import pg from 'pg';
import dateFormat from 'dateformat';

//var pg = require('pg');
//var dateFormat = require('dateformat');

class DatabaseInterface {

  constructor() {
    this.connectionString = process.env.DATABASE_URL || 'postgres://postgres:postgres@localhost:5432/mars';
    this.client = new pg.Client(this.connectionString);
  }

  // Get data for a particular date
  getDataForDate(searchDate, callback) {
    var results = [];

    this.client.connect();

    var stringDate = dateFormat(searchDate, 'yyyy-mm-dd');
    var query = this.client.query("SELECT * FROM marsData WHERE date = to_date($1, 'yyyy-mm-dd')", [stringDate]);

    query.on('row', function(row) {
      results.push(row);
    });

    var cl = this.client;
    var inter = this;
    query.on('end', function() {
      cl.end();
      if(results.length >= 1) {
        var model = inter.convertRowToModel(results[0]);
      }
      else {
        var model = new TemperatureModel();
      }
      callback(model);
    });
  }

  // Insert data for a particular date
  insertData(dataModel) {
    this.client.connect();

    var query = this.client.query("INSERT INTO marsData (temperature, date, sky) values($1, $2, $3)",
                                  [dataModel.temperature, dataModel.date, dataModel.sky]);

    var cl = this.client;
    query.on('end', function() { cl.end(); });
  }

  // Converts a row returned from the SQL query into a model object
  convertRowToModel(row) {
    var model = new TemperatureModel();
    model.temperature = row.temperature;
    model.sky = row.sky;
    model.date = dateFormat(row.date, 'yyyy-mm-dd')
    model.valid = true;

    return model;
  }
}

module.exports.DatabaseInterface = DatabaseInterface;
