class TemperatureModel {
  constructor() {
    this.date = "";
    this.temperature = 0;
    this.sky = "";
    this.valid = false;
  }
}

module.exports.TemperatureModel = TemperatureModel;
