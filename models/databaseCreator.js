var pg = require('pg');
var connectionString = process.env.DATABASE_URL || 'postgres://postgres:postgres@localhost:5432/mars';

var client = new pg.Client(connectionString);
client.connect();
var query = client.query('CREATE TABLE marsData(id SERIAL PRIMARY KEY, temperature REAL, date DATE, sky CHAR(50))');
query.on('end', function() { client.end(); });
