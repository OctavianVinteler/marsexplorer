import {DataRetriever} from '../src/dataRetriever'
import {DatabaseInterface} from '../src/databaseInterface'
import {TemperatureModel} from '../models/temperatureModel'

var express = require('express');
var router = express.Router();

var dataRetriever = new DataRetriever();
var databaseInterface = new DatabaseInterface();

var dataModel = new TemperatureModel();

/* GET home page. */
router.get('/', function(req, res, next) {

  //dataRetriever.obtainInformation(processData);
  // First look for the current date in the database
  databaseInterface.getDataForDate(new Date(), updateData);
  res.render('index', { title: 'Express' });
});

// Process obtain data
function updateData(result) {
  console.log(result);
}

module.exports = router;
