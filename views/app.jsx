import React from 'react';
import Content from './content.jsx'
import ContentContainer from '../src/container'

class App extends React.Component {

  constructor() {
    super();

    this.state = {
      date: "",
      sky: "",
      temperature: 0
    }
  };

  handleStateChange(temperatureModel) {
    if(temperatureModel.valid) {
      this.setState({date: temperatureModel.date, sky: temperatureModel.sky, temperature: temperatureModel.temperature});
    }
    else {
      this.setState({date: "N/A", sky: "N/A", temperature: 0});
    }
  };

  render() {
    return (
      <div>
        <ContentContainer />
      </div>
    );
  };
}

export default App;
