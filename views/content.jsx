import React from 'react';

class Content extends React.Component {
  render() {
    return (
      <div>
        <h2>{this.props.date}</h2>
        <h2>{this.props.temperature}</h2>
        <h2>{this.props.sky}</h2>
      </div>
    );
  }
}

Content.propTypes = {
   date: React.PropTypes.string.isRequired,
   temperature: React.PropTypes.number.isRequired,
   sky: React.PropTypes.string.isRequired
}

export default Content;
